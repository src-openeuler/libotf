Name:           libotf
Version:        0.9.16
Release:        2
Summary:        Library for supporting processing of OpenType fonts
License:        LGPL-2.1-or-later
URL:            https://www.nongnu.org/m17n/
Source0:        https://download.savannah.gnu.org/releases/m17n/%{name}-%{version}.tar.gz
Patch0:         libotf-0.9.16-replace-freetype-config.patch
BuildRequires:  gcc
BuildRequires:  autoconf automake libtool
BuildRequires:  pkgconfig(freetype2)
BuildRequires:  libXaw-devel

%description
Library for handling OpenType fonts,especially those needed for CJK and other non-Latin.

%package      devel
Summary:      Development support for libotf library
Requires:     %{name} = %{version}-%{release} pkgconfig

%description  devel
Development headers and libraries for libotf.

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -fi
%configure --disable-static
%make_build

%install
%make_install
%delete_la

(cd example && make clean && rm -rf .deps && rm Makefile)

%files
%license COPYING
%doc AUTHORS README NEWS
%{_libdir}/*.so.*
%{_bindir}/otfdump
%{_bindir}/otflist
%{_bindir}/otftobdf
%{_bindir}/otfview

%files devel
%doc example
%{_bindir}/libotf-config
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Wed Sep 11 2024 Funda Wang <fundawang@yeah.net> - 0.9.16-2
- fix build with recent freetype

* Mon Oct 09 2023 wulei <wulei80@h-partners.com> - 0.9.16-1
- Update to 0.9.16

* Fri Nov 15 2019 duyeyu <duyeyu@huawei.com> - 0.9.13-13
- Package init
